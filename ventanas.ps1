$validDestinations = @('work', 'pcmasterrace')
$destinationMachine=$args[0]

if([string]::IsNullOrWhiteSpace($destinationMachine)) {
	Write-Host 'Destination machine not specified'
	Exit 1
}

if($validDestinations.contains($destinationMachine)) {
	& "$PSScriptRoot\packages-all.ps1"
	& "$PSScriptRoot\scripts\$destinationMachine\packages.ps1"
}
else {
	Write-Host 'Not a known destination machine'
	Exit 1
}

Exit