An attempt to automate and document all my Windows configurations.

# Instructions

1. Install [Chocolatey](https://docs.chocolatey.org/en-us/choco/setup)
2. Run the following instructions on a privileged PowerShell session

```
Set-ExecutionPolicy Bypass -Scope Process
.\ventanas.ps1
```

# Policies

## Global

- Make sure to have Windows Defender enabled. You don't need anything else.
- For package management, use Chocolate and its [package repository](https://community.chocolatey.org/packages)

## Work computer

- Only a minimal selection of packages might be installed here. Try to install everything else under your **Work Machine**

## Gaming computer

- It should run Windows LTSC because it's less bloated.